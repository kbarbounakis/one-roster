import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class DepartmentReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('Department');
    const findAttribute = model.fields.find((field) => {
      return field.name === 'oneRoster'
    });
    if (findAttribute == null) {
      model.fields.push({
        "name": "oneRoster",
        "type": "OneRosterDepartmentLink",
        "many": true,
        "nested": true,
        "multiplicity": "ZeroOrOne",
        "mapping": {
            "associationType": "association",
            "cascade": "delete",
            "parentModel": "Department",
            "parentField": "id",
            "childModel": "OneRosterDepartmentLink",
            "childField": "department",
        }
      });
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
    DepartmentReplacer
}
