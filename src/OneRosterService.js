import { Args, ApplicationService } from '@themost/common';
import { DataObjectState, ODataModelBuilder } from '@themost/data';
import { oneRosterRouter } from './oneRosterRouter';

import {OneRosterAcademicSessionProvider} from './models/OneRosterAcademicSession';
import {OneRosterOrgProvider} from './models/OneRosterOrg';
import {OneRosterCourseProvider} from './models/OneRosterCourse';
import {OneRosterClassProvider} from './models/OneRosterClass';
import {OneRosterEnrollmentProvider} from './models/OneRosterEnrollment';
import {OneRosterUserProvider} from './models/OneRosterUser';
import { OneRosterLineItemProvider } from './models/OneRosterLineItem';
import './extensions/DataModel';
import { OneRosterResultProvider } from './models/OneRosterResult';
import { StudentReplacer } from './StudentReplacer';
import { CourseReplacer } from './CourseReplacer';
import { DepartmentReplacer } from './DepartmentReplacer';
import { CourseClassReplacer } from './CourseClassReplacer';
import { InstituteReplacer } from './InstituteReplacer';
import { OneRosterNoLineItemProvider } from './models/OneRosterLineItem';
import { OneRosterNoResultProvider } from './models/OneRosterResult';
import { OneRosterReplaceClassProvider } from './models/OneRosterReplaceClass';
import { InstructorReplacer } from './InstructorReplacer';
import { OneRosterLogger } from './models/OneRosterLogger';
import { OneRosterDivideClassProvider } from './models/OneRosterDivideClass';
import { CourseClassInstructorReplacer } from './CourseClassInstructorReplacer';
import { CourseClassSectionReplacer } from './CourseClassSectionReplacer';
import { StudentCourseClassReplacer } from './StudentCourseClassReplacer';
import { UserReplacer } from './UserReplacer';

const en = require('../locales/en.json');
const el = require('../locales/el.json');

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
 function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

class OneRosterService extends ApplicationService {
    /**
     * 
     * @param {ExpressDataApplication} app 
     */
    constructor(app) {
        super(app);
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container != null) {
                    // append translation keys
                    const translateService = app.getService(function TranslateService() {});
                    if (translateService) {
                        translateService.setTranslation('en', en);
                        translateService.setTranslation('el', el);
                    }
                    container.use('/ims/oneroster/v1p1/', oneRosterRouter(app));
                    // get container router
                    const router = container._router;
                    // find data context middleware
                    const before = router.stack.find((item) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    const insert = router.stack[router.stack.length - 1];
                    // re-index router
                    insertRouterBefore(router, before, insert);
                }
            });
        }

        Object.defineProperty(this, '_services', {
            configurable: true,
            enumerable: false,
            writable: false,
            value: {}
        });

        // register services
        const registerServices = [
            OneRosterAcademicSessionProvider,
            OneRosterOrgProvider,
            OneRosterCourseProvider,
            OneRosterUserProvider,
            OneRosterClassProvider,
            OneRosterEnrollmentProvider,
            [
                OneRosterLineItemProvider,
                OneRosterNoLineItemProvider
            ],
            [
                OneRosterResultProvider,
                OneRosterNoResultProvider
            ],
            [
                OneRosterReplaceClassProvider,
                OneRosterReplaceClassProvider
            ],
            [
                OneRosterDivideClassProvider,
                OneRosterDivideClassProvider
            ]
        ];
        registerServices.forEach((RegisterServiceCtor) => {
            if (Array.isArray(RegisterServiceCtor)) {
                const [ServiceCtor, StrategyCtor] = RegisterServiceCtor;
                if (app.hasService(ServiceCtor)) {
                    // get application service instance
                    const serviceInstance = app.getService(ServiceCtor)
                    // and use it as a strategy here
                    this.useStrategy(ServiceCtor, function () { return serviceInstance; })
                } else {
                    // otherwise, use the default strategy constructor
                    this.useStrategy(ServiceCtor, StrategyCtor);
                }
            } else {
                if (app.hasService(RegisterServiceCtor)) {
                    const serviceInstance = app.getService(RegisterServiceCtor)
                    this.useStrategy(RegisterServiceCtor, function () { return serviceInstance; });
                } else {
                    this.useService(RegisterServiceCtor);
                }
            }
        });

        new StudentReplacer(app).apply();
        new CourseReplacer(app).apply();
        new CourseClassReplacer(app).apply();
        new DepartmentReplacer(app).apply();
        new InstituteReplacer(app).apply();
        new InstructorReplacer(app).apply();
        new CourseClassInstructorReplacer(app).apply();
        new CourseClassSectionReplacer(app).apply();
        new StudentCourseClassReplacer(app).apply();
        new UserReplacer(app).apply();

        /**
         * get model builder
         * @type {ODataModelBuilder}
         */
        const builder = app.getStrategy(ODataModelBuilder);
        builder.clean(true);
        builder.getEdmDocument().then(() => {
            // do nothing
        });

    }

    hasService(serviceCtor) {
        if (serviceCtor == null) {
            return false;
        }
        Args.check(typeof serviceCtor === 'function', new Error('Strategy constructor is invalid.'));
        return Object.prototype.hasOwnProperty.call(this._services, serviceCtor.name);
    }
    getService(serviceCtor) {
        if (serviceCtor == null) {
            return false;
        }
        Args.check(typeof serviceCtor === 'function', new Error('Strategy constructor is invalid.'));
        if (Object.prototype.hasOwnProperty.call(this._services, serviceCtor.name) === false) {
            return;
        }
        return this._services[serviceCtor.name];
    }
    useStrategy(serviceCtor, strategyCtor) {
        if (strategyCtor == null) {
            return false;
        }
        Args.check(typeof serviceCtor === 'function', new Error('Service constructor is invalid.'));
        Args.check(typeof strategyCtor === 'function', new Error('Strategy constructor is invalid.'));
        Object.defineProperty(this._services, serviceCtor.name, {
            configurable: true,
            enumerable: true,
            writable: true,
            value: new strategyCtor(this)
        });
        return this;
    }
    useService(serviceCtor) {
        return this.useStrategy(serviceCtor, serviceCtor);
    }
    getConfiguration() {
        return this.application.getConfiguration();
    }

    /**
     * 
     * @param {DataContext} context 
     * @param {OneRosterSyncEvent} event 
     * @param {{mode:string}=} event 
     */
    async sync(context, event, options) {
        const opts = Object.assign({ mode: 'full' }, options);
        // get sync mode which can be 'full' or 'partial'
        const { mode: syncMode } = opts;
        /**
         * @type {import('./models/OneRosterSyncAction').OneRosterSyncAction}
         */
        let initiator;
        if (event.emitter && event.emitter.additionalType) {
            initiator = context.model(event.emitter.additionalType).convert(event.emitter);
        }
        const logger = initiator ? initiator.logger : new OneRosterLogger();
        
        // sync enumerations
        await context.model('OneRosterClassType').asQueryable().getItems();
        await context.model('OneRosterGender').asQueryable().getItems();
        await context.model('OneRosterImportanceType').asQueryable().getItems();
        await context.model('OneRosterOrgType').asQueryable().getItems();
        await context.model('OneRosterRoleType').asQueryable().getItems();
        await context.model('OneRosterScoreStatusType').asQueryable().getItems();
        await context.model('OneRosterSessionType').asQueryable().getItems();
        await context.model('OneRosterStatusType').asQueryable().getItems();
        if (syncMode === 'full') {
            // get academic sessions and orgs before continuing with other providers
            // because these two services provide global entities available for any department
            await Promise.sequence([
                OneRosterAcademicSessionProvider,
                OneRosterOrgProvider
            ].map((ProviderCtor) => {
                return async () => {
                    const results = await this.getService(ProviderCtor).preSync(context, event.filter);
                    const [[entityType, items]] = results;
                    let index = 0;
                    let count = items.length;
                    await context.model(entityType).on('before.save', (event, callback) => {
                        index++;
                        if (index % 10 === 0) {
                            logger.log(context, 'OneRosterSyncAction', event.filter, `(${entityType}) Updating ${index} of ${count} item(s)`);
                        }
                        return callback();
                    }).silent().upsert(items);
                    if (count) {
                        logger.log(context, 'OneRosterSyncAction', event.filter, `(${entityType}) Finished updating ${count} item(s)`);
                    }
                }
            }));
        }
        // continue with all the other one roster providers based on the departments included into the filter
        const departments = [];
        if (Array.isArray(event.filter.departments)) {
            departments.push(...event.filter.departments);
        } else {
            departments.push(event.filter.department);
        }
        let outputResults = [];
        for (const department of departments) {
            await context.db.executeInTransactionAsync(async () => {
                const { academicYear, academicPeriod } = event.filter;
                const filter = { academicYear, academicPeriod, department };
                if (Object.prototype.hasOwnProperty.call(event.filter, 'courseCode')) {
                    filter.courseCode = event.filter.courseCode;
                }
                if (Object.prototype.hasOwnProperty.call(event.filter, 'classCode')) {
                    filter.classCode = event.filter.classCode;
                }
                // get org
                const { name: departmentName } = await context.model('Department').where('id').equal(department).select('name').getItem();
                logger.log(context, 'OneRosterSyncAction', filter, `Synchronizing department ${departmentName} for academic year ${academicYear} and academic period ${academicPeriod}`);
                outputResults = [];
                await Promise.sequence([
                    OneRosterCourseProvider,
                    OneRosterClassProvider,
                    OneRosterUserProvider,
                    OneRosterEnrollmentProvider,
                    OneRosterReplaceClassProvider,
                    OneRosterDivideClassProvider,
                    OneRosterLineItemProvider,
                    OneRosterResultProvider,
                ].map((ProviderCtor) => {
                    return async () => {
                        const serviceInstance = this.getService(ProviderCtor);
                        Args.check(serviceInstance != null, new Error(`${ProviderCtor.name} instance cannot be found.'`));
                        // get service constructor and create a new instance
                        const { constructor: Provider } = serviceInstance;
                        /**
                         * @type {OneRosterProvider}
                         */
                        const service = new Provider(this.getApplication());
                        // set logger
                        service.logger = logger;
                        // subscribe to filtering event
                        await service.filtering.subscribe(async (event) => {
                            // if entity type is available, then push items
                            if (event.entityType) {
                                // get output result
                                const outputResult = outputResults.find((x) => x[0] === event.entityType);
                                if (outputResult) {
                                    const [, items] = outputResult;
                                    // set items
                                    event.items = items;
                                }
                            }
                        });
                        if (typeof service.sync === 'function') {
                            const syncResults = await service.sync(context, filter);
                            if (Array.isArray(syncResults)) {
                                for (const [entityType, items] of syncResults) {
                                    // push items to output for further processing
                                    const outputResult = outputResults.find((x) => x[0] === entityType);
                                    if (outputResult) {
                                        // append output items
                                        outputResult[1].push(...items);
                                    } else {
                                        // push new output result
                                        outputResults.push([entityType, items]);
                                    }
                                }
                            }
                            return;
                        }
                        const results = await this.getService(ProviderCtor).preSync(context, filter);

                        for (const [entityType, items] of results) {
                            // find item with state 'deleted'
                            const deleteItems = items.filter((x) => x.$state === DataObjectState.Delete);
                            const upsertItems = items.filter((x) => x.$state !== DataObjectState.Delete);
                            if (deleteItems.length) {
                                let indexDelete = 0;
                                let totalDelete = deleteItems.length;
                                const stepDelete = totalDelete > 1000 ? 100 : totalDelete > 100 ? 50 : 10;
                                context.model(entityType).on('before.remove', (event, callback) => {
                                    indexDelete++;
                                    if (indexDelete % stepDelete === 0) {
                                        logger.log(context, 'OneRosterSyncAction', filter, `(${entityType}) Removing ${indexDelete} of ${totalDelete} item(s)`);
                                    }
                                    return callback();
                                }).silent().remove(deleteItems);
                            }
                            let index = 0;
                            let total = upsertItems.length;
                            const step = total > 1000 ? 100 : total > 100 ? 50 : 10;
                            await context.model(entityType).on('before.save', (event, callback) => {
                                index++;
                                if (index % step === 0) {
                                    logger.log(context, 'OneRosterSyncAction', filter, `(${entityType}) Updating ${index} of ${total} item(s)`);
                                }
                                return callback();
                            }).silent().upsert(upsertItems);
                            // push items to output for further processing
                            const outputResult = outputResults.find((x) => x[0] === entityType);
                            if (outputResult) {
                                // append output items
                                outputResult[1].push(...upsertItems);
                            } else {
                                // push new output result
                                outputResults.push([entityType, upsertItems]);
                            }
                            if (total) {
                                logger.log(context, 'OneRosterSyncAction', filter, `(${entityType}) Finished updating ${total} item(s)`);
                            }
                        }
                    }
                }));
                logger.log(context, 'OneRosterSyncAction', filter, `Finished synchronizing department ${departmentName} for academic year ${academicYear} and academic period ${academicPeriod}`);
            });
        }
        if (departments.length === 1) {
            // return output results for single department
            return outputResults;
        }
        return [];
    }


}


export {
    OneRosterService
}