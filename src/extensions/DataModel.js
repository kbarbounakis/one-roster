import { DataModel, DataObjectState } from '@themost/data';
import { DataError } from '@themost/common';
import '@themost/promise-sequence';

class DataModelExtension extends DataModel {
    /**
     * @param obj {*|Array<*>}
     * @param callback {Function}
     * @returns {void|Promise<*>}
     */
    upsert(obj, callback) {
        if (typeof callback === 'undefined') {
            return this.upsertAsync(obj);
        } else {
            return this.upsertAsync(obj).then((result) => {
                return callback(null, result);
            }).catch((err) => {
                return callback(err);
            });
        }
    }
    /**
     * @param {*|Array<*>} obj
     * @returns {Promise<*>}
     */
    upsertAsync(obj) {
        let items = [];
        const self = this;
        // create a clone
        const thisModel = this.clone(this.context);
        // format object(s) to array
        if (Array.isArray(obj)) {
            items = obj;
        } else {
            items.push(obj);
        }
        return Promise.sequence(items.map(function(item) {
            return function() {
                if (thisModel.primaryKey == null) {
                    throw new DataError('E_PKEY', 'Primary key cannot be empty at this context', null, thisModel.name);
                }
                if (item[thisModel.primaryKey] == null) {
                    delete item[thisModel.primaryKey];
                    return new Promise(function(resolve, reject) {
                        self.inferState(item, function(err, state) {
                            if (err) {
                                return reject(err);
                            }
                            Object.assign(item, {
                                $state: state
                            });
                            return resolve(item);
                        });
                    });
                }
                // try to find object by primary key
                return thisModel.where(thisModel.primaryKey).equal(item[thisModel.primaryKey]).silent().count().then(function(result) {
                    // if object does not exist, set state to insert
                    Object.assign(item, {
                        $state: (result === 0) ? DataObjectState.Insert : DataObjectState.Update
                    });
                    // and return item
                    return Promise.resolve(item);
                });
            }
        })).then(function() {
            // finally do update
            return self.save(items).then(function(results) {
                // delete $state
                items.forEach(function (item) {
                    delete item.$state;
                });
                return Array.isArray(obj) ? results : results[0];
            });
        });
    }
}

if (typeof DataModel.prototype.upsert === 'undefined') {
    Object.defineProperty(DataModel.prototype, 'upsert', {
        configurable: true,
        enumerable: true,
        value: DataModelExtension.prototype.upsert
    });
    Object.defineProperty(DataModel.prototype, 'upsertAsync', {
        configurable: true,
        enumerable: true,
        value: DataModelExtension.prototype.upsertAsync
    });
}