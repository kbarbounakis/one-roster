import { stringify as uuidStringify } from 'uuid';
import { parse as uuidParse } from 'uuid';

class OneRosterUtils {

    static integerToGuid(num, format) {
        if (typeof num != 'number') {
            throw new Error('Exected number');
        }
        const intBytes = OneRosterUtils.getBytes(num);
        const bytes = uuidParse(format || '00000000-0000-1000-8000-000000000000');
        for (let index = 0; index < intBytes.length; index++) {
            bytes[index] = intBytes[index];
        }
        return uuidStringify(bytes, 0);
    }

    static getBytes(num) {
        let b = new ArrayBuffer(4);
        new DataView(b).setUint32(0, num);
        return Array.from(new Uint8Array(b));
    }

    /**
     * @param {number} num 
     * @param {string=} format zero-pad formatted string e.g. 00000000 or 0000
     * @returns {string}
     */
    static toHexString(num, format) {
        const bytes = OneRosterUtils.getBytes(num);
        if (format == null) {
            return bytes.map((byte) => byte.toString(16)).join('');
        }
        else {
            return (format + bytes.map((byte) => byte.toString(16)).join('')).slice(-format.length);
        }
    }

}

export {
    OneRosterUtils
}