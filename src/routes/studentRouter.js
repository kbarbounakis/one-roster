import {Router}  from 'express';
import { Guid } from '@themost/common';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import {promisify} from 'util';
import { NIL as NIL_UUID } from 'uuid';
import { TotalCount, OneRosterResponseFormatter } from './entitySetRouter';

// eslint-disable-next-line no-unused-vars
function studentRouter(app) {
    const router = Router();
    router.get('/', async function getAllStudents(req, res, next) {
        try {
            /**
             * @type {DataModel}
             */
            const model = req.context.model('OneRosterUser');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {DataQueryable}
             */
            const query = await filterAsync(req.query);
            // get items
            const results = await query.prepare().and('role').equal('student').silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });
    router.get('/:id', async function getStudent(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;
            /**
             * @type {DataModel}
             */
            const model = req.context.model('OneRosterUser');
            const item = await model.where('sourcedId').equal(id)
                .and('role').equal('student')
                .silent().getItem();
            res.set(TotalCount, item != null ? 1 : 0);
            return res.json(new OneRosterResponseFormatter().for(model).format(item));
        } catch (err) {
            return next(err);
        }
    });

    router.get('/:id/classes', async function getClassesForStudent(req, res, next) {
        try {
            const id = Guid.isGuid(req.params.id) ? req.params.id : NIL_UUID;

            const model = req.context.model('OneRosterClass');
            const filterAsync = promisify(model.filter).bind(model);
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const query = await filterAsync(req.query);

            const classes = new QueryEntity(model.viewAdapter);
            const enrollments = new QueryEntity(req.context.model('OneRosterEnrollment').sourceAdapter).as('enrollments');
            query.select();
            query.query.join(enrollments).with(
                new QueryExpression().where(
                    new QueryField('class').from(enrollments)
                ).equal(
                    new QueryField('sourcedId').from(classes)
                ).and(
                    new QueryField('user').from(enrollments)
                ).equal(
                    id
                ).and(
                    new QueryField('role').from(enrollments)
                ).equal(
                    'student'
                )
            )
            const results = await query.silent().getList();
            res.set(TotalCount, results.total);
            return res.json(new OneRosterResponseFormatter().for(model).format(results.value));
        } catch (err) {
            return next(err);
        }
    });
    return router;
}

export {
    studentRouter
}