import {Router}  from 'express';
import { entitySetRouter } from './entitySetRouter';

function categoryRouter(app) {
    const router = Router();
    router.use(entitySetRouter(app, 'OneRosterLineItemCategory'));
    return router;
}

function lineItemRouter(app) {
    const router = Router();
    router.use(entitySetRouter(app, 'OneRosterLineItem'));
    return router;
}

export {
    categoryRouter,
    lineItemRouter
}