import {Router}  from 'express';
import { entitySetRouter } from './entitySetRouter';

function resultRouter(app) {
    const router = Router();
    router.use(entitySetRouter(app, 'OneRosterResult'));
    return router;
}

export {
    resultRouter
}