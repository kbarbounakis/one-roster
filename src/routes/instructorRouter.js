import { AccessDeniedError, HttpForbiddenError, LangUtils } from '@themost/common';
import {Router}  from 'express';
import { DataModel } from '@themost/data';
const { parseBoolean } = LangUtils;

if (typeof DataModel.prototype.filterAsync === 'undefined') {
    DataModel.prototype.filterAsync = function(queryParams) {
        return new Promise((resolve, reject) => {
            this.filter(queryParams, (err, result) => {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    };
}

function instructorRouter() {
    const router = Router();

    function tryInstructor() {
        return (req, res, next) => {
            const {DataObjectClass: Instructor} = req.context.model('Instructor');
            void Instructor.getMe(req.context).then(getInstructor => {
                return getInstructor.getItem().then(({id: instructor}) => {
                    if (!instructor) {
                        return next(new AccessDeniedError('The current user does not have access to complete this action.'));
                    }
                    Object.assign(req.params, {
                        instructor
                    });
                    return next();
                });
            }).catch((err) => {
                return next(err);
            });
        };
    }

    function tryCourseClass() {
        return (req, _res, next) => {
           req.context.model('CourseClassInstructor')
            .where('instructor').equal(req.params.instructor)
            .and('courseClass').equal(req.params.id).count().then((exists) => {
                if (parseBoolean(exists) === false) {
                    return next(new AccessDeniedError('The current user does not have access to complete this action'));
                }
                return next();
            }).catch((err) => {
                return next(err);
            });
        };
    }

    router.get('/me/classes/:classCode/oneroster/descendants', tryInstructor(), async (req, res, next) => {
        try {
            // check if the user is an instructor of the given class
            const instructor = await req
                .context
                .model('Instructor')
                .where('user/name')
                .equal(req.context.user.name)
                .getTypedItem();
            
            const instructorClass = await (await instructor
                .getInstructorClasses()
                .select('id', 'sections')
                .where('id')
                .equal(req.params.classCode)
                .expand(
                    {
                        "name": "sections",
                        "options": {
                            "$select": "id, sectionIndex"
                        }
                    }
                )
                .getItem());

            // throw a forbidden error if he is not
            if (!instructorClass) {
                throw new HttpForbiddenError('User is not an instructor for the specified class.');
            }
            let results = [];
            // make the queries silently if he is
            // get the main OneRosterClass
            const mainClass = await req
                .context
                .model('OneRosterClass')
                .where('classCode')
                .equal(req.params.classCode)
                .silent()
                .getItem();

            results.push(mainClass);

            // if the course has sections
            if (instructorClass.sections && instructorClass.sections.length > 0) {
                // get the section OneRosterClasses
                const sectionClasses = await req
                    .context
                    .model('OneRosterClass')
                    .where('classCode')
                    .in(instructorClass.sections.map(section => `${req.params.classCode}-${section.sectionIndex}`))
                    .orderBy('classCode')
                    .silent()
                    .getItems();

                results.push(...sectionClasses);
            }

            // check if the class is in replace relationships
            const merges = await req
                .context
                .model('OneRosterReplaceClass')
                .select('id', 'courseClass', 'replacedBy')
                .where('replacedBy')
                .equal(req.params.classCode)
                .or('courseClass')
                .equal(req.params.classCode)
                .getItems();

            if (merges) {
                // get the replace OneRosterClasses
                const mergedClasses = await req
                    .context
                    .model('OneRosterClass')
                    .where('classCode')
                    .in(merges.map(merge => merge.courseClass === req.params.classCode ? merge.replacedBy : merge.courseClass))
                    .orderBy('classCode')
                    .silent()
                    .getItems();

                results.push(...mergedClasses);
            }
            
            // if the class doesn't have sections
            if (!(instructorClass.sections && instructorClass.sections.length > 0)) {
                // check if the class has divisions
                const divisions = await req
                    .context
                    .model('OneRosterDivideClass')
                    .select('id', 'classIndex')
                    .where('courseClass')
                    .equal(req.params.classCode)
                    .getItems()

                if (divisions) {
                    // get the division OneRosterClasses
                    const divisionClasses = await req
                        .context
                        .model('OneRosterClass')
                        .where('classCode')
                        .in(divisions.map(division => `${req.params.classCode}-${division.classIndex}`))
                        .orderBy('classCode')
                        .silent()
                        .getItems();

                    results.push(...divisionClasses);
                }
            }

            // find the number of student enrollments for each OneRosterClass
            const enrollments = await req.context
                .model('OneRosterEnrollment')
                .select('class/sourcedId as class', 'count(user) as studentEnrollments')
                .where('class')
                .in(results.map(oneRosterClass => oneRosterClass.sourcedId))
                .and('role')
                .equal('student')
                .groupBy('class/sourcedId')
                .silent()
                .getItems();
            
            // assign the number to the each class
            for (const oneRosterClass of results) {
                const enrollment = enrollments.filter(enrollment => enrollment.class === oneRosterClass.sourcedId);
                oneRosterClass.studentEnrollments = enrollment.length > 0 ? enrollment[0].studentEnrollments : 0;
            }

            return res.json(results);
        } catch(err) {
            return next(err);
        }
    });

    router.get('/me/classes/oneroster/replacements', tryInstructor(), async (req, res, next) => {
        try {
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const q = await req.context.model('OneRosterReplaceClass').filterAsync(req.query);
            const value = await q.prepare().where('courseClass/instructors/instructor').equal(req.params.instructor).getItems();
            return res.json({
                value
            });
        } catch(err) {
            return next(err);
        }
    });

    router.delete('/me/classes/oneroster/replacements', tryInstructor(), async (req, res, next) => {
        try {
            const items = Array.isArray(req.body) ? req.body : [req.body];
            const results = await req.context.model('OneRosterReplaceClass').remove(items);
            return res.json(results);
        } catch(err) {
            return next(err);
        }
    });

    router.post('/me/classes/oneroster/replacements', tryInstructor(), async (req, res, next) => {
        try {
            const items = Array.isArray(req.body) ? req.body : [req.body];
            const results = await req.context.model('OneRosterReplaceClass').save(items);
            return res.json(results);
        } catch (err) {
            return next(err);
        }
    });

    router.get('/me/classes/:classCode/oneroster/classes', tryInstructor(), async (req, res, next) => {
        try {
            /**
             * @type {import('@themost/data').DataQueryable}
             */
            const q = await req.context.model('OneRosterDivideClass').filterAsync(Object.assign(req.query, {
                $orderby: 'classIndex'
            }));
            const value = await q.prepare().where('courseClass').equal(req.params.classCode)
                .and('courseClass/instructors/instructor').equal(req.params.instructor)
                .getItems();
            return res.json({
                value
            });
        } catch(err) {
            return next(err);
        }
    });

    router.post('/me/classes/:classCode/oneroster/classes', tryInstructor(), async (req, res, next) => {
        try {
            await req.context.db.executeInTransactionAsync(async () => {
                let payload = Array.isArray(req.body) ? req.body : [req.body];
                const items = payload.map((item) => {
                    delete item.id;
                    return Object.assign(item, {
                        courseClass: req.params.classCode
                    });
                });
                const results = await req.context.model('OneRosterDivideClass').save(items);
                return res.json(results);
            });
        } catch(err) {
            return next(err);
        }
    });

    router.get('/me/classes/:id/oneroster/indivisible', tryInstructor(), tryCourseClass(), async (req, res, next) => {
        try {
            const item = await req.context.model('OneRosterIndivisibleClass')
                .where('courseClass').equal(req.params.id)
                .select('indivisible','letSectionActive').getItem();
            return res.json(item)
        } catch(err) {
            return next(err);
        }
    });

    router.post('/me/classes/:id/oneroster/indivisible', tryInstructor(), tryCourseClass(), async (req, res, next) => {
        try {
            const { id: courseClass } = req.params; 
            const item = {
                courseClass
            }
            if (req.body.indivisible != null) {
                item.indivisible = req.body.indivisible;
            }
            if (req.body.letSectionActive != null) {
                item.letSectionActive = req.body.letSectionActive;
            }

            await req.context.model('OneRosterIndivisibleClass').save(item);
            return res.json(item);
        } catch(err) {
            return next(err);
        }
    });
    
    return router;
}

export {
    instructorRouter
}
