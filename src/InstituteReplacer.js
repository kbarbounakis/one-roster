import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class InstituteReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('Institute');
    const findAttribute = model.fields.find((field) => {
      return field.name === 'oneRoster'
    });
    if (findAttribute == null) {
      model.fields.push({
        "name": "oneRoster",
        "type": "OneRosterInstituteLink",
        "many": true,
        "nested": true,
        "multiplicity": "ZeroOrOne",
        "mapping": {
            "associationType": "association",
            "cascade": "delete",
            "parentModel": "Institute",
            "parentField": "id",
            "childModel": "OneRosterInstituteLink",
            "childField": "institute",
        }
      });
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
    InstituteReplacer
}
