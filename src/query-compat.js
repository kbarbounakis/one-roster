import { QueryField, QueryEntity } from '@themost/query';

if (typeof QueryField.prototype.from === 'function') {
    const superFrom = QueryField.prototype.from;
    // try to fix QueryField.from method in order to support QueryEntity as argument
    // important note: this operation is supported by @themost/query@2.5.23 or later
    if (/if\s+\(entity\sinstanceof\sQueryEntity\)/gm.test(superFrom.toString()) === false) {
        QueryField.prototype.from = function (entity) {
            if (entity instanceof QueryEntity) {
                const fromEntity = entity.$as || entity.name;
                return superFrom.call(this, fromEntity);
            }
            return superFrom.call(this, entity);
        };
    }
}