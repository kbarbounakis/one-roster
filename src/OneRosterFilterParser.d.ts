import { OpenDataParser } from '@themost/query';

export declare class OneRosterFilterParser extends OpenDataParser {
    getString(): string;
}