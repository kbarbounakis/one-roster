import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class StudentCourseClassReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('StudentCourseClass');
    model.eventListeners = model.eventListeners || [];
    let found = model.eventListeners.find((item) => item.type === '@themost/data/previous-state-listener');
    if (found == null) {
      model.eventListeners.unshift({
        "type": "@themost/data/previous-state-listener"
      });
    }
    const type = path.resolve(__dirname, './listeners/OnCourseClass');
    found = model.eventListeners.find((item) => item.type === type);
    if (found == null) {
      model.eventListeners.push({
        type
      });
    }
    schemaLoader.setModelDefinition(model);
  }
}

export {
  StudentCourseClassReplacer
}
