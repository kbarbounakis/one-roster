import { EdmMapping } from '@themost/data';
import { template } from 'lodash'
import {OneRosterBaseType} from './OneRosterBaseType';
import '@themost/promise-sequence';
import {OneRosterAcademicSession} from './OneRosterAcademicSession';
import { LangUtils } from '@themost/common';
import { QueryEntity, QueryExpression, QueryField, SqlFormatter } from '@themost/query';
import { OneRosterProvider } from './OneRosterProvider';
import { OneRosterUserProvider } from './OneRosterUser';
import { OneRosterCourseProvider } from './OneRosterCourse';
const {parseBoolean} = LangUtils;

class OneRosterAppendReplaceClassProvider extends OneRosterProvider {
    constructor(context) {
        super(context);
    }
    /**
     * 
     * @param {*} context 
     * @param {{academicYear: number, academicPeriod: number, classCode: string, classes: Array<any>}} filter 
     * @returns 
     */
    async preSync(context, filter) {
        const OneRosterReplaceClasses = context.model('OneRosterReplaceClass').silent();
        // get class
        const currentClass = await context.model('CourseClass').where('id').equal(filter.classCode)
            .select(
                'id as classCode',
                'title as title',
                'year/id as academicYear',
                'period/id as academicPeriod'
            ).getItem();
        // get replaced classes
        const replacedClasses = await OneRosterReplaceClasses.where('replacedBy').equal(filter.classCode)
            .select('courseClass').getItems();
        const replacedByClass = await OneRosterReplaceClasses.where('courseClass').equal(filter.classCode)
            .select('courseClass').getItem();
        if (replacedByClass) {
            replacedClasses.push(replacedByClass);
        }
        const users = [];
        const courses = [];
        const classes = [];
        if (replacedClasses.length > 0) {
            this.logger.info(context, 'OneRosterSyncAction', filter, `Selecting classes that replace or have been replaced by (${currentClass.classCode}) ${currentClass.title}`);
        }
        // loop through replaced classes
        for (const replacedClass of replacedClasses) {
            let exists = null;
            if (Array.isArray(filter.classes)) {
                exists = filter.classes.find((x) => x.classCode === replacedClass.courseClass);
                if (exists) {
                    continue;
                }
            }
            exists = classes.find((x) => x.classCode === replacedClass.courseClass);
            if (exists == null) {
                // create filter
                const extraFilter = {
                    academicYear: filter.academicYear,
                    academicPeriod: filter.academicPeriod,
                    classCode: replacedClass.courseClass
                };
                const classProvider = this.getApplication().getService(OneRosterClassProvider);
                // call preSync method to forcibly get data
                const extraResults = await classProvider.preSync(context, extraFilter, {
                    force: true,
                    initiator: this // set initiator to current provider (this assignment is required for excluding circular operations)
                });
                // check if there are extra classes and get first item
                const oneRosterClassResults = extraResults.find((x) => x[0] === 'OneRosterClass');
                if (oneRosterClassResults) {
                    const [, extraClasses] = oneRosterClassResults;
                    // if a class is found
                    if (extraClasses.length) {
                        // add replaced classes to current set
                        classes.push(...extraClasses);
                        // enumerate extra classes
                        for (const extraClass of extraClasses) {
                            let userProvider = this.getApplication().getService(OneRosterUserProvider);
                            if (userProvider == null) {
                                userProvider = new OneRosterUserProvider(this.getApplication());
                            }
                            const extraUsers = await userProvider.preSync(context, {
                                academicYear: filter.academicYear,
                                academicPeriod: filter.academicPeriod,
                                classCode: extraClass.classCode
                            });
                            const oneRosterUserResults = extraUsers.find((x) => x[0] === 'OneRosterUser');
                            if (oneRosterUserResults) {
                                const [, extraUsers] = oneRosterUserResults;
                                // add users to current set
                                const addUsers = extraUsers.filter((item) => {
                                    return users.find((x) => x.username === item.username) == null;
                                });
                                users.push(...addUsers);
                            }
                            let courseProvider = this.getApplication().getService(OneRosterCourseProvider);
                            if (courseProvider == null) {
                                courseProvider = new OneRosterCourseProvider(this.getApplication());
                            }
                            const extraCourses = await courseProvider.preSync(context, {
                                academicYear: filter.academicYear,
                                academicPeriod: filter.academicPeriod,
                                courseCode: extraClass.course.courseCode
                            });
                            const oneRosterCourseResults = extraCourses.find((x) => x[0] === 'OneRosterCourse');
                            if (oneRosterCourseResults) {
                                const [, extraCourses] = oneRosterCourseResults;
                                // add courses to current set
                                const addCourses = extraCourses.filter((item) => {
                                    return courses.find((x) => x.courseCode === item.courseCode) == null;
                                });
                                courses.push(...addCourses);
                            }
                        }
                    }
                }
            }
        }
        if (replacedClasses.length > 0) {
            this.logger.info(context, 'OneRosterSyncAction', filter, `Finished selecting classes that replace or have been replaced by (${currentClass.classCode}) ${currentClass.title}`);
        }
        return [
            [
                'OneRosterUser',
                users
            ],
            [
                'OneRosterCourse',
                courses
            ],
            [
                'OneRosterClass',
                classes
            ]
        ]
    }
}


@EdmMapping.entityType('OneRosterClass')
class OneRosterClass extends OneRosterBaseType {
    /**
     * @type {string}
     */
    title;
    /**
     * @type {OneRosterAcademicSession}
     */
    schoolYear;
    /**
     * @type {string}
     */
     courseCode;
    /**
     * @type {Array<*>}
     */
    subjects;

    constructor() {
        super();
    }

    

}

class OneRosterClassProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    /**
     * @param {DataContext} context 
     * @param {OneRosterProviderFilter} filter
     */
     async preSync(context, filter) {

        const q = context.model('CourseClass').select(
            'id',
            'id as classCode',
            'title as title',
            'year/id as academicYear',
            'period/id as academicPeriod',
            'course/id as course',
            'course/displayCode as displayCode',
            'course/department/id as department',
            'status/alternateName as status',
            'statistic/inLanguage as inLanguage',
            'dateModified'
            ).where('year').equal(filter.academicYear).and('period').equal(filter.academicPeriod);
        // get department filter
        if (filter.department) {
            q.and('course/department').equal(filter.department);
        }

        const results = await Promise.sequence([
            () => q.expand('locales').silent().take(-1).getItems()
        ]);

        // get default locale
        let defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
        const translateService = context.application.getStrategy(function TranslateService() { });
        const titleTemplate = template(
            context.getConfiguration().getSourceAt('settings/universis/one-roster/classTitleFormat') ||
            '${title}')

        const classes = results[0].map((item) => {
            const inLanguageLocale = item.locales.find(x => x.inLanguage == item.inLanguage);
            if (inLanguageLocale?.title) item.title = inLanguageLocale.title;
            const status = item.status === 'cancelled' ? 'inactive' : 'active'
            item.status = status;
            item.statusDesc = status === 'active' ? '' : translateService.translate(status);
            const title = titleTemplate(item).trim();
            return {
                status: item.status,
                dateLastModified: item.dateModified || new Date(),
                type: 'scheduled',
                title: title,
                classCode: item.classCode.toString(),
                course: {
                    courseCode: item.course
                },
                school: {
                    identifier: `DEPT-${item.department}`
                },
                terms: [
                    {
                        sourcedId: OneRosterAcademicSession.generateId(item.academicYear, item.academicPeriod)
                    }
                ],
                metadata: {
                    locales: item.locales.map(({inLanguage, title}) => {
                        return {
                            inLanguage,
                            title
                        };
                    })
                }
            }
        }).map((courseClass) => {
            if (defaultLocale) {
                const find = courseClass.metadata.locales.find((locale) => locale.inLanguage === defaultLocale);
                if (find == null) {
                    courseClass.metadata.locales.push({
                        inLanguage: defaultLocale,
                        title: courseClass.title
                    });
                }
            }
            return courseClass;
        });

        return [
            [
                'OneRosterClass',
                classes
            ]
        ];
    }
}

class OneRosterClassWithSectionsProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    /**
     * @param {DataContext} context 
     * @param {OneRosterProviderFilter} filter
     * @param {{force?: boolean, description?: string, initiator?:*}=} options
     */
     async preSync(context, filter, options) {

        const { force, initiator } = Object.assign({}, { force: false }, options);

        const CourseClass = context.model('CourseClass');
        /**
         * @type {import('@themost/data').DataQueryable}
         */
        const queryClasses = CourseClass.select(
                'id',
                'id as classCode',
                'title as title',
                'year/id as academicYear',
                'year/name as academicYearName',
                'period/id as academicPeriod',
                'period/name as academicPeriodName',
                'course/id as course',
                'course/displayCode as displayCode',
                'course/department/id as department',
                'status/alternateName as status',
                'statistic/inLanguage as inLanguage',
                'dateModified'
            ).where('year').equal(filter.academicYear).and('period').equal(filter.academicPeriod);
        // get department filter
        if (filter.department) {
            queryClasses.and('course/department').equal(filter.department);
        }
        // if filter contains a classCode
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            // append filter for a single class
            queryClasses.and('id').equal(filter.classCode);
        }
        await context.model('OneRosterIndivisibleClass').migrateAsync();
        const { viewAdapter: OneRosterIndivisibleClasses } = context.model('OneRosterIndivisibleClass');
        const { viewAdapter: CourseClassLocales } = context.model('CourseClassLocale');
        const { viewAdapter: CourseClasses } = CourseClass;

        queryClasses.query.leftJoin(new QueryEntity(OneRosterIndivisibleClasses)).with(
            new QueryExpression().where(
                new QueryField('id').from(CourseClasses)
            ).equal(
                new QueryField('courseClass').from(OneRosterIndivisibleClasses)
            )
        );
        // get title based on the language of instruction
        // important note: get title from course locales instead of course class locales
        // todo: get also course class title using language of instruction
        // PSEUDO SQL: LEFT JOIN CourseLocales ON CourseClasses.course = CourseLocales.object 
        // AND statistic.inLanguage = CourseLocales.inLanguage
        queryClasses.query.leftJoin(new QueryEntity(CourseClassLocales)).with(
            new QueryExpression().where(
                new QueryField('id').from(CourseClasses)
            ).equal(
                new QueryField('object').from(CourseClassLocales)
            ).and(
                new QueryField('inLanguage').from(CourseClassLocales)
            ).equal(
                new QueryField('inLanguage').from('statistic')
            )
        );
        const selectClasses = queryClasses.query.$select[CourseClasses];
        selectClasses.push(new QueryField('indivisible').from(OneRosterIndivisibleClasses).as('indivisible'));
        selectClasses.push(new QueryField('letSectionActive').from(OneRosterIndivisibleClasses).as('letSectionActive'));
        selectClasses.push(new QueryField('title').from(CourseClassLocales).as('inLanguageTitle'));
        
        // get academic session identifier
        const academicSession = OneRosterAcademicSession.generateId(filter.academicYear, filter.academicPeriod);
        // query one roster classes
        const queryOneRosterClasses = context.model('OneRosterClass').select(
            'classCode',
            'dateLastModified'
        ).where('terms/sourcedId').equal(academicSession)
        if (filter.department) {
            queryOneRosterClasses.and('school/identifier').equal(`DEPT-${filter.department}`);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            queryOneRosterClasses.and('classCode').equal(filter.classCode);
        }
        const CourseClassSection = context.model('CourseClassSection')
        const queryClassSections = CourseClassSection.select(
                'id',
                'name',
                'section',
                'courseClass/title as title',
                'courseClass/year/id as academicYear',
                'courseClass/year/name as academicYearName',
                'courseClass/period/id as academicPeriod',
                'courseClass/period/name as academicPeriodName',
                'courseClass/course/id as course',
                'courseClass/course/displayCode as displayCode',
                'courseClass/course/department/id as department',
                'courseClass/statistic/inLanguage as inLanguage',
                'courseClass as classCode',
                'courseClass/dateModified as dateModified'
            ).where('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);
        // get department filter
        if (filter.department) {
            queryClassSections.and('courseClass/course/department').equal(filter.department);
        }
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            queryClassSections.and('courseClass').equal(filter.classCode);
        }

        // add inLanguageTitle
        queryClassSections.query.leftJoin(new QueryEntity(CourseClassLocales)).with(
            new QueryExpression().where(
                new QueryField('id').from('courseClass')
            ).equal(
                new QueryField('object').from(CourseClassLocales)
            ).and(
                new QueryField('inLanguage').from(CourseClassLocales)
            ).equal(
                new QueryField('inLanguage').from('statistic')
            )
        );
        const selectClassSections = queryClassSections.query.$select[CourseClassSection.viewAdapter];
        selectClassSections.push(new QueryField('title').from(CourseClassLocales).as('inLanguageTitle'));


        const sqlFormatter = context.application.getService(SqlFormatter);
        if (sqlFormatter) {
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryClasses', sqlFormatter.format(queryClasses.query));
            // this.logger.debug(context, 'OneRosterSyncAction', filter, 'queryClassSections', sqlFormatter.format(queryClassSections.query));
        }

        if (filter.classCode == null) {
            this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting classes ...');
        }
        const results = await Promise.sequence([
            () => queryClasses.expand('locales').silent().take(-1).getItems(),
            () => queryClassSections.silent().take(-1).getItems(),
            () => queryOneRosterClasses.silent().take(-1).getItems()
        ]);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results[0].length} class(es) and ${results[1].length} class section(s)`);

        // get default locale
        let defaultLocale = context.getConfiguration().getSourceAt('settings/i18n/defaultLocale');
        const translateService = context.application.getStrategy(function TranslateService() { });
        const titleTemplate = template(
            context.getConfiguration().getSourceAt('settings/universis/one-roster/classTitleFormat') ||
            '${title}')

        const [, courseClassSections, oneRosterClasses] = results;
        // filter classes with no class sections
        const classes = results[0].filter((item) => {
            // if sync process is forced then return all classes
            if (force) {
                return true
            }
            // filter and exclude course classes based on date modified
            const existing = oneRosterClasses.find((x) => x.classCode === item.classCode.toString());
            if (existing && existing.dateLastModified >= item.dateModified) {
                return false;
            }
            return true;
        }).map((item) => {
            // check if class has sections and is not indivisible
            const setInactive = !item.indivisible && (courseClassSections.filter((x) => x.classCode === item.classCode).length > 0);
            item.title = item.inLanguageTitle || item.title;
            const status = item.status === 'cancelled' ? 'inactive' : (setInactive ? 'inactive' : 'active');
            item.status = status;
            item.statusDesc = status === 'active' ? '' : translateService.translate(status);
            const title = titleTemplate(item).trim();
            return {
                status: item.status,
                dateLastModified: item.dateModified || new Date(),
                type: 'scheduled',
                title: title,
                classCode: item.classCode.toString(),
                course: {
                    courseCode: item.course
                },
                school: {
                    identifier: `DEPT-${item.department}`
                },
                terms: [
                    {
                        sourcedId: OneRosterAcademicSession.generateId(item.academicYear, item.academicPeriod)
                    }
                ],
                metadata: {
                    locales: item.locales.map(({inLanguage, title}) => {
                        return {
                            inLanguage,
                            title
                        };
                    })
                }
            }
        }).map((courseClass) => {
            if (defaultLocale) {
                const find = courseClass.metadata.locales.find((locale) => locale.inLanguage === defaultLocale);
                if (find == null) {
                    courseClass.metadata.locales.push({
                        inLanguage: defaultLocale,
                        title: courseClass.title
                    });
                }
            }
            return courseClass;
        });

        const users = [];
        const courses = [];
        if (initiator == null || initiator.constructor.name !== 'OneRosterAppendReplaceClassProvider') {
            // for each class in current set of classes
            const replaceClassProvider = new OneRosterAppendReplaceClassProvider(this.getApplication());
            for (const oneRosterClass of classes) {
                const intermediateResults = await replaceClassProvider.preSync(context, {
                    academicYear: filter.academicYear,
                    academicPeriod: filter.academicPeriod,
                    classCode: oneRosterClass.classCode
                }, {
                    force: true
                });
                let intermediateResult = intermediateResults.find((x) => x[0] === 'OneRosterClass');
                if (intermediateResult) {
                    const [, extraClasses] = intermediateResult;
                    const appendClasses = extraClasses.filter((item) => {
                        return classes.find((x) => x.classCode === item.classCode) == null;
                    });
                    if (appendClasses.length) { 
                        // get courses and users
                        intermediateResult = intermediateResults.find((x) => x[0] === 'OneRosterCourse');
                        if (intermediateResult) {
                            const [, extraCourses] = intermediateResult;
                            courses.push(...extraCourses);
                        }
                        intermediateResult = intermediateResults.find((x) => x[0] === 'OneRosterUser');
                        if (intermediateResult) {
                            const [, extraUsers] = intermediateResult;
                            const addUsers = extraUsers.filter((item) => {
                                return users.find((x) => x.username === item.username) == null;
                            });
                            users.push(...addUsers);
                        }
                        // add replaced classes to current set
                        classes.push(...appendClasses);
                        this.logger.info(context, 'OneRosterSyncAction', filter, `Finished including replaced class(es) for (${oneRosterClass.classCode}) ${oneRosterClass.title}`);
                    }
                }
            }
        }

        // get class section title format (or use default)
        const classSectionTitleFormat = context.getConfiguration().getSourceAt('settings/universis/one-roster/classSectionTitleFormat');
        const classSectionTitleTemplate = template(classSectionTitleFormat || '${title} - ${name}');
        
        const addClasses = courseClassSections.filter((item) => {
            // filter and exclude course class section based on date modified
            const existing = oneRosterClasses.find((x) => x.classCode === `${item.classCode}-${item.section}`);
            if (existing && existing.dateLastModified >= item.dateModified) {
                return false;
            }
            return true;
        }).map((item) => {
            // find class
            const courseClass = results[0].find((x) => x.classCode === item.classCode);
            // get status based on parent class status
            let status = courseClass.status === 'cancelled' ? 'inactive' : 'active';
            // check if parent class is indivisible
            if (courseClass && courseClass.indivisible && status === 'active') {
                // deactivate class section if parent class is indivisible
                status = parseBoolean(courseClass.letSectionActive) ? 'active' : 'inactive';
            }
            item.status = status;
            item.statusDesc = status === 'active' ? '' : translateService.translate(status);
            item.title = item.inLanguageTitle || item.title;
            return {
                status,
                dateLastModified: courseClass.dateModified || new Date(),
                type: 'scheduled',
                title: classSectionTitleTemplate(item).trim(), // format title
                classCode: `${courseClass.classCode}-${item.section}`,
                course: {
                    courseCode: courseClass.course
                },
                school: {
                    identifier: `DEPT-${courseClass.department}`
                },
                terms: [
                    {
                        sourcedId: OneRosterAcademicSession.generateId(courseClass.academicYear, courseClass.academicPeriod)
                    }
                ],
                metadata: {
                    locales: courseClass.locales.map(({inLanguage, title}) => {
                        return {
                            inLanguage,
                            title: `${title} - ${item.name}`
                        };
                    })
                }
            }
        });

        // add sections
        classes.push(...addClasses);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Returning ${classes.length} class(es) to insert or update`);
        const res = [
            [
                'OneRosterCourse',
                courses
            ],
            [
                'OneRosterClass',
                classes
            ],
            [
                'OneRosterUser',
                users
            ]
        ];
        return res;
    }
}

export {
    OneRosterClass,
    OneRosterClassWithSectionsProvider,
    OneRosterClassProvider,
    OneRosterAppendReplaceClassProvider
};
