import { EdmMapping } from '@themost/data';
import {OneRosterBaseType} from './OneRosterBaseType';
import '@themost/promise-sequence';
import { OneRosterUtils } from '../OnRosterUtils';
import moment from 'moment';
import { OneRosterProvider } from './OneRosterProvider';

@EdmMapping.entityType('OneRosterAcademicSession')
class OneRosterAcademicSession extends OneRosterBaseType {
    /**
     * @type {string}
     */
    title;
    /**
     * @type {Date}
     */
    startDate;
    /**
     * @type {Date}
     */
    endDate;
    /**
     * @type {string}
     */
     type;
    /**
     * @type {OneRosterAcademicSession}
     */
     parent;
     /**
     * @type {Array<OneRosterAcademicSession>}
     */
    children;
    constructor() {
        super();
    }

    /**
     * @param {number} academicYear 
     * @param {number=} academicPeriod 
     */
    static generateId(academicYear, academicPeriod) {
        if (academicPeriod == null) {
            return `${OneRosterUtils.toHexString(academicYear, '00000000')}-0000-1000-8001-000000000000`;
        } else {
            const strAcademicPeriod = OneRosterUtils.toHexString(academicPeriod, '00000000');
            const strAcademicYear = OneRosterUtils.toHexString(academicYear, '0000');
            return `${strAcademicPeriod}-${strAcademicYear}-1000-8001-000000000000`;
        }
    }

    /**
     * @param {number} academicYear 
     * @param {number} examPeriod 
     */
    static generateGradingPeriodId(academicYear, examPeriod) {
        const arg1 = OneRosterUtils.toHexString(examPeriod, '00000000');
        const arg2 = OneRosterUtils.toHexString(academicYear, '0000');
        return `${arg1}-${arg2}-1000-8002-000000000000`;
    }

}

class OneRosterAcademicSessionProvider extends OneRosterProvider {

    constructor(app) {
        super(app);
    }

    /**
     * @param {DataContext} context 
     * @param {OneRosterProviderFilter} filter
     */
     async preSync(context, filter) {
        // get academic years
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting academic years ...');
        const results1 = await context.model('AcademicYear').where('id').equal(filter.academicYear).silent().take(-1).getItems();
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results1.length} academic years`);
        const academicYears = results1.map((item) => {
            return {
                sourcedId: OneRosterAcademicSession.generateId(item.id),
                status: 'active',
                type: 'schoolYear',
                dateLastModified: item.dateModified || new Date(),
                title: item.name,
                startDate: moment(new Date(item.id, 8, 1)).format('YYYY-MM-DD'),
                endDate: moment(new Date(item.id + 1, 7, 31)).format('YYYY-MM-DD'),
                schoolYear: item.id
            }
        });
        // validate 
        // get academic periods
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting academic periods ...');
        const results2 = await context.model('AcademicPeriod').silent().take(-1).getItems();
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${results2.length} academic periods`);
        const academicPeriods = [];
        for (const academicYear of academicYears) {
            const items = results2.map((item) => {
                return {
                    sourcedId: OneRosterAcademicSession.generateId(academicYear.schoolYear, item.id),
                    status: 'active',
                    dateLastModified: item.dateModified || new Date(),
                    type: 'semester',
                    title: `${academicYear.title} ${item.name}`,
                    startDate: academicYear.startDate,
                    endDate: academicYear.endDate,
                    parent: academicYear.sourcedId,
                    schoolYear: academicYear.schoolYear
                }
            });
            academicPeriods.push.apply(academicPeriods, items);
        }
        this.logger.info(context, 'OneRosterSyncAction', filter, 'Getting exam periods ...');
        const examPeriods = await context.model('ExamPeriod').silent().take(-1).getItems();
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${examPeriods.length} exam periods`);
        const academicYearExamPeriods = [];
        for (const academicYear of academicYears) {
            const items = examPeriods.map((item) => {
                return {
                    sourcedId: OneRosterAcademicSession.generateGradingPeriodId(academicYear.schoolYear, item.id),
                    status: academicYear.status,
                    dateLastModified: item.dateModified || new Date(),
                    type: 'gradingPeriod',
                    title: `${academicYear.title} ${item.alternateName}`,
                    startDate: academicYear.startDate,
                    endDate: academicYear.endDate,
                    parent: academicYear.sourcedId,
                    schoolYear: academicYear.schoolYear
                }
            });
            academicYearExamPeriods.push.apply(academicYearExamPeriods, items);
        }

        const academicSessions = [].concat(academicYears, academicPeriods, academicYearExamPeriods);
        this.logger.info(context, 'OneRosterSyncAction', filter, `Found ${academicSessions.length} academic sessions`);
        return [
            [
                'OneRosterAcademicSession',
                academicSessions
            ]
        ];
    }
}

export {
    OneRosterAcademicSession,
    OneRosterAcademicSessionProvider
};
