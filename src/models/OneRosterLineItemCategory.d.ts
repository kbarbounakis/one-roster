import { OneRosterBaseType } from './OneRosterBaseType';

export declare class OneRosterLineItemCategory extends OneRosterBaseType {
    title: string;
}
