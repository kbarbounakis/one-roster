import { DataObject } from '@themost/data';


class OneRosterGuidRef extends DataObject {
    /**
     * @type {string}
     */
    sourcedId;
    /**
     * @type {string}
     */
    type;
    /**
     * @type {string}
     */
    href;
    constructor() { 
        super();
    }

    /**
     * @param {string} attribute 
     */
    static selectFrom(attribute) {
        return `${attribute}($select=sourcedId,baseType as type,href)`
    }

}

export {
    OneRosterGuidRef
};