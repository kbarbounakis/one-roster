import { EdmMapping } from '@themost/data';
import { OneRosterBaseType } from './OneRosterBaseType';
import { OneRosterAcademicSession } from './OneRosterAcademicSession';
import { LangUtils } from '@themost/common';
import moment from 'moment';
import '@themost/promise-sequence';
import { OneRosterProvider } from './OneRosterProvider';
const parseBoolean = LangUtils.parseBoolean;

@EdmMapping.entityType('OneRosterResult')
class OneRosterResult extends OneRosterBaseType {
    /**
     * @type {import('./OneRosterLineItem').OneRosterLineItem}
     */
    lineItem;
    /**
     * @type {import('./OneRosterUser').OneRosterUser}
     */
    student;
    /**
     * @type {*}
     */
     scoreStatus;
    /**
     * @type {number}
     */
     score;
    /**
     * @type {string}
     */
     scoreDate;
    /**
    * @type {string}
    */
     comment;
    

    constructor() {
        super();
    }

}

class OneRosterResultProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    /**
         * @param {DataContext} context 
         * @param {OneRosterProviderFilter} filter
         */
    async preSync(context, filter) {

        /**
         * @type {import('@themost/data').DataQueryable}
         */
        const q = context.model('StudentGrade').select(
            'courseClass/id as classCode',
            'courseClass/year/id as academicYear',
            'courseExam/examPeriod/id as examPeriod',
            'student/user/name as username',
            'grade1 as score',
            'formattedGrade as scoreText',
            'gradeModified as scoreDate',
            'isPassed as passed',
            'dateModified as dateModified'
        ).where('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);
        // get department filter
        if (filter.department) {
            q.and('courseClass/course/department').equal(filter.department);
        }
        // get items
        const items = await q.silent().take(-1).getItems();
        const lineItems = [];
        for (const item of items) {
            // get lineItem
            const gradingPeriod = OneRosterAcademicSession.generateGradingPeriodId(item.academicYear, item.examPeriod);
            const lineItem = await context.model('OneRosterLineItem')
                .where('category/title').equal('final')
                .and('class/classCode').equal(item.classCode)
                .and('gradingPeriod').equal(gradingPeriod)
                .select('sourcedId').silent().value();
            lineItems.push({
                lineItem: lineItem,
                scoreStatus: 'submitted',
                student: {
                    username: item.username
                },
                score: item.score != null ? item.score * 100 : null,
                scoreDate: item.scoreDate != null ? moment(item.scoreDate).format('YYYY-MM-DD') : null,
                metadata: {
                    scoreText: item.scoreText,
                    passed: parseBoolean(item.passed)
                }
            })
        }
        return [
            [
                'OneRosterResult',
                lineItems
            ]
        ];
    }

}


class OneRosterNoResultProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterResult',
                []
            ]
        ];
    }
}

export {
    OneRosterResult,
    OneRosterResultProvider,
    OneRosterNoResultProvider
}