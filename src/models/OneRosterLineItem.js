import { EdmMapping } from '@themost/data';
import { OneRosterBaseType } from './OneRosterBaseType';
import { OneRosterAcademicSession } from './OneRosterAcademicSession';
import '@themost/promise-sequence';
import { OneRosterProvider } from './OneRosterProvider';
@EdmMapping.entityType('OneRosterLineItem')
class OneRosterLineItem extends OneRosterBaseType {
    /**
     * @type {string}
     */
    title;
    /**
     * @type {string}
     */
    description;
    /**
     * @type {Date}
     */
    assignDate;
    /**
     * @type {Date}
     */
    dueDate;
    /**
     * @type {import('./OneRosterClass').OneRosterClass}
     */
    class;
    /**
    * @type {import('./OneRosterLineItemCategory').OneRosterLineItemCategory}
    */
    category;
    /**
    * @type {import('./OneRosterAcademicSession').OneRosterAcademicSession|string}
    */
    gradingPeriod;
    /**
    * @type {number}
    */
    resultValueMin;
    /**
     * @type {number}
     */
    resultValueMax;

    constructor() {
        super();
    }

}

class OneRosterLineItemProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    /**
         * @param {DataContext} context 
         * @param {OneRosterProviderFilter} filter
         */
    async preSync(context, filter) {

        /**
         * @type {import('@themost/data').DataQueryable}
         */
        const q = context.model('CourseExamClass').select(
            'courseClass/id as classCode',
            'courseClass/course/name as title',
            'courseClass/year/id as academicYear',
            'courseExam/gradeScale as gradingScheme',
            'courseExam/examPeriod/id as examPeriod',
            'courseExam/description as description',
            'courseExam/status/alternateName as status',
            'courseExam/dateCompleted as dateCompleted',
            'courseExam/resultDate as resultDate',
            'courseExam/dateModified as dateModified'
        ).where('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod);
        // get department filter
        if (filter.department) {
            q.and('courseClass/course/department').equal(filter.department);
        }
        const items = await q.silent().take(-1).getItems();
        const lineItems = items.map((item) => {
            return {
                status: 'active',
                dateLastModified: item.dateModified || new Date(),
                assignDate: item.resultDate || item.dateCompleted || item.dateModified,
                dueDate: item.resultDate || item.dateCompleted || item.dateModified,
                gradingPeriod: OneRosterAcademicSession.generateGradingPeriodId(item.academicYear, item.examPeriod),
                category: {
                    title: 'final'
                },
                class: {
                    classCode: item.classCode
                },
                title: item.title,
                description: item.description,
                metadata: {
                    gradingScheme: item.gradingScheme
                }
            }
        });
        return [
            [
                'OneRosterLineItem',
                lineItems
            ]
        ];
    }

}

class OneRosterNoLineItemProvider extends OneRosterProvider {
    constructor(app) {
        super(app);
    }
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterLineItem',
                []
            ]
        ];
    }
}

export {
    OneRosterLineItem,
    OneRosterLineItemProvider,
    OneRosterNoLineItemProvider
}