import { DataObject, EdmMapping } from '@themost/data';
import { OneRosterProvider } from './OneRosterProvider';
import { QueryEntity, QueryExpression, QueryField } from '@themost/query';
import { Guid } from '@themost/common';
import { template } from 'lodash';

@EdmMapping.entityType('OneRosterDivideClass')
class OneRosterDivideClass extends DataObject {
    
}

class OneRosterDivideClassProvider extends OneRosterProvider {
    constructor(context) {
        super(context);
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    async preSync(context, filter) {
        return [
            [
                'OneRosterEnrollment',
                []
            ]
        ]
    }

    /**
     * @param {import('@themost/data').DataContext} context 
     * @param {{academicYear: number, academicPeriod: number, department: string | number, classCode?: string | number }} filter 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    async sync(context, filter) {

        const { viewAdapter: OneRosterDivideClasses } = context.model('OneRosterDivideClass');
        const { sourceAdapter: OneRosterClasses } = context.model('OneRosterClass');
        const { sourceAdapter: OneRosterEnrollments } = context.model('OneRosterEnrollment');
        const oneRosterClass = new QueryEntity(OneRosterClasses).as('oneRosterClass');

        this.logger.info(context, 'OneRosterSyncAction', filter, `Dividing classes`);

        // get class parts
        // pseudo SQL: SELECT * FROM OneRosterDivideClass WHERE 
        // academicYear = ? AND academicPeriod = ? AND department = ? AND classCode = ?
        const queryClasses = context.model('OneRosterDivideClass')
            .where('courseClass/year').equal(filter.academicYear)
            .and('courseClass/period').equal(filter.academicPeriod)
        // if (filter.department) {
        //     queryClasses.and('courseClass/course/department').equal(filter.department);
        // }
        if (Object.prototype.hasOwnProperty.call(filter, 'classCode')) {
            queryClasses.and('courseClass').equal(filter.classCode);
        }
        queryClasses.select(
            'id',
            'courseClass/id as courseClass',
            'title as name',
            'classIndex',
            'status',
            'courseClass/title as title',
            'courseClass/year/id as academicYear',
            'courseClass/year/name as academicYearName',
            'courseClass/period/id as academicPeriod',
            'courseClass/period/name as academicPeriodName',
            'courseClass/course/id as course',
            'courseClass/course/displayCode as displayCode',
            'courseClass/course/department/id as department',
            'courseClass/statistic/inLanguage as inLanguage',
            'courseClass as classCode',
            'courseClass/dateModified as dateModified'
        ).expand('instructors');
        queryClasses.query.join(oneRosterClass).with( 
            // pseudo SQL: JOIN OneRosterClass AS courseClass ON courseClass.classCode = OneRosterReplaceClass.courseClass
            new QueryExpression().where(
                new QueryField('courseClass').from(OneRosterDivideClasses)
            ).equal(
                new QueryField('classCode').from(oneRosterClass)
            )
        );
        // add inLanguageTitle
        const { viewAdapter: CourseClassLocales } = context.model('CourseClassLocale');
        queryClasses.query.leftJoin(new QueryEntity(CourseClassLocales)).with(
            new QueryExpression().where(
                new QueryField('courseClass').from(OneRosterDivideClasses)
            ).equal(
                new QueryField('object').from(CourseClassLocales)
            ).and(
                new QueryField('inLanguage').from(CourseClassLocales)
            ).equal(
                new QueryField('inLanguage').from('statistic')
            )
        );
        /**
         * @type {Array<QueryField>}
         */
        const selectClasses = queryClasses.query.$select[OneRosterDivideClasses];
        selectClasses.push(new QueryField('title').from(oneRosterClass).as('classTitle'));
        selectClasses.push(new QueryField('sourcedId').from(oneRosterClass).as('oneRosterClass'));
        selectClasses.push(new QueryField('title').from(CourseClassLocales).as('inLanguageTitle'));

        const divideClasses = await queryClasses.getAllItems();
        if (divideClasses.length === 0) {
            return;
        }
        // get in-process classes
        const event = {
            target: this,
            entityType: 'OneRosterClass',
            items: [],
        }
        await this.filtering.emit(event);
        let dividing = 0;

        const translateService = context.application.getStrategy(function TranslateService() { });
        // get class part title format
        const classPartTitleFormat = context.getConfiguration().getSourceAt('settings/universis/one-roster/classPartTitleFormat');
        const classPartTitleTemplate = template(classPartTitleFormat || '${title} (${name})');
        const titleTemplate = template(
            context.getConfiguration().getSourceAt('settings/universis/one-roster/classTitleFormat') ||
            '${title}')

        const output = [
            [
                'OneRosterClass',
                []
            ]
        ];

        for (const item of event.items) {
            const itemDivideClasses = divideClasses.filter((x) => x.courseClass === item.classCode);
            if (itemDivideClasses.length === 0) {
                continue;
            }
            for (const divideClass of itemDivideClasses) {
                dividing++;
                divideClass.title = divideClass.inLanguageTitle || divideClass.title;
                divideClass.status = divideClass.status || 'active'; 
                divideClass.statusDesc = divideClass.status === 'active' ? '' : translateService.translate(divideClass.status);
                const { oneRosterClass, classIndex, title, name, courseClass, dateModified, status  } = divideClass;
                // create or update new class
                const newClassCode = `${courseClass}-${classIndex}`;
                let oneRosterClassItem = await context.model('OneRosterClass').where('classCode').equal(newClassCode).getItem();
                if (oneRosterClassItem) {
                    this.logger.info(context, 'OneRosterSyncAction', filter, `Updating class part #${classIndex}.  ${title} (${name})`);
                    // update class
                    Object.assign(oneRosterClassItem, {
                        title: classPartTitleTemplate(divideClass).trim(),
                        status: status,
                        dateLastModified: dateModified
                    });
                    await context.model('OneRosterClass').update(oneRosterClassItem);
                } else {
                    this.logger.info(context, 'OneRosterSyncAction', filter, `Creating class part #${classIndex}.  ${title} (${name})`);
                    // get parent class
                    oneRosterClassItem = await context.model('OneRosterClass')
                        .where('sourcedId').equal(oneRosterClass).expand(
                            'terms'
                        ).getItem();
                    // remove attributes
                    delete oneRosterClassItem.sourcedId;
                    delete oneRosterClassItem.href;
                    delete oneRosterClassItem.metadata;
                    // update attributes
                    Object.assign(oneRosterClassItem, {
                        classCode: newClassCode,
                        title: classPartTitleTemplate(divideClass).trim(),
                        status: status,
                        dateLastModified: dateModified
                    });
                    // and save new class
                    await context.model('OneRosterClass').insert(oneRosterClassItem);
                }
                const updated = await context.model('OneRosterClass').where('sourcedId').equal(oneRosterClassItem.sourcedId).getItem();
                output[0][1].push(updated);
                // get enrollments of the parent class
                const enrollments = await context.model('OneRosterEnrollment')
                    .where('class/classCode').equal(courseClass).flatten().getAllItems();
                // delete existing enrollments (for this class part)
                const deleteEnrollments = new QueryExpression().delete(OneRosterEnrollments).where('class').equal(oneRosterClassItem.sourcedId);
                await context.db.executeAsync(deleteEnrollments);
                // add student enrollments when status 
                const origin = context.getApplication().getConfiguration().getSourceAt('settings/universis/one-roster/origin') || '/ims/oneroster/v1p1/';
                if (item.status !== 'inactive') {
                    const studentEnrollments = enrollments.filter((x) => x.role === 'student');
                    this.logger.info(context, 'OneRosterSyncAction', filter, `Updating or creating ${studentEnrollments.length} student enrollment(s) of class part #${classIndex}. ${title} (${name})`);
                    for (const enrollment of studentEnrollments) {
                        const sourcedId = Guid.newGuid().toString();
                        const href = `${origin}enrollments/${sourcedId}`;
                        const insertEnrollment = Object.assign({}, enrollment, {
                            'sourcedId': sourcedId,
                            'class': oneRosterClassItem.sourcedId,
                            'href': href,
                            'primary': false,
                            'baseType': 'enrollment'
                        });
                        await context.db.executeAsync(
                            new QueryExpression().insert(insertEnrollment).into(OneRosterEnrollments)
                        );
                    }
                }
                // add instructor enrollments
                const instructors = divideClass.instructors || [];
                this.logger.info(context, 'OneRosterSyncAction', filter, `Updating or creating ${instructors.length} teacher enrollment(s) of class part #${classIndex}.  ${title} (${name})`);
                for (const instructor of instructors) {
                    // get OneRoster user
                    const username = await context.model('Instructor').where('id').equal(instructor.id).select('user/name').value();
                    const user = await context.model('OneRosterUser').where('username').equal(username).getItem();
                    if (user) {
                        const sourcedId = Guid.newGuid().toString();
                        const href = `${origin}enrollments/${sourcedId}`;
                        const insertEnrollment = {
                            'user': user.sourcedId,
                            'class': oneRosterClassItem.sourcedId,
                            'school': oneRosterClassItem.school.sourcedId,
                            'role': 'teacher',
                            'primary': true,
                            'sourcedId': sourcedId,
                            'baseType': 'enrollment',
                            'href': href,
                            'status': 'active',
                            'dateLastModified': oneRosterClassItem.dateLastModified
                        };
                        await context.db.executeAsync(
                            new QueryExpression().insert(insertEnrollment).into(OneRosterEnrollments)
                        );
                    }
                    
                }
                // deactivate parent class
                // pseudo SQL: UPDATE OneRosterClass SET status = 'inactive' WHERE sourcedId = ?
                // filter items
                const setInactive = itemDivideClasses.filter((x) => x.status === 'active').length;
                if (setInactive) {
                    divideClass.status = 'inactive';
                    divideClass.statusDesc = translateService.translate('inactive');
                    await context.db.executeAsync(new QueryExpression().update(OneRosterClasses).set({
                        status: 'inactive',
                        title: titleTemplate(divideClass).trim()
                    }).where('sourcedId').equal(item.sourcedId));
                }
            }
        }
        if (dividing > 0) {
            this.logger.info(context, 'OneRosterSyncAction', filter, `Finished dividing ${dividing} class(es)`);    
        } else {
            this.logger.info(context, 'OneRosterSyncAction', filter, `The operation did not find any class to divide`);    
        }
        return output;
    }

}

export {
    OneRosterDivideClass,
    OneRosterDivideClassProvider
}