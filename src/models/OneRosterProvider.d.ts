import { ApplicationService } from '@themost/common';
import { OneRosterLogger } from './OneRosterLogger';
import { AsyncEventEmitter } from '@themost/events';

export declare class OneRosterProvider extends ApplicationService {
    logger: OneRosterLogger;
    filtering: AsyncSeriesEventEmitter<{ target: OneRosterProvider, entityType: string, items: Array<any> }>
}