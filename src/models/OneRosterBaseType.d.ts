import { DataObject } from "@themost/data";

export declare interface OneRosterProviderFilter {
    academicYear: any;
    academicPeriod: any;
    department?: any;
    departments?: any[];
    courseCode?: string;
    classCode?: string;
}

export declare class OneRosterBaseType extends DataObject {
    //
}