import { EdmMapping } from '@themost/data';
import { OneRosterBaseType } from './OneRosterBaseType';

@EdmMapping.entityType('OnRosterLineItem')
class OneRosterLineItemCategory extends OneRosterBaseType {
    /**
     * @type {string}
     */
    title;
}

export {
    OneRosterLineItemCategory
}