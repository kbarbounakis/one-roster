import { ApplicationService, ApplicationBase } from '@themost/common';
import { DataObject } from '@themost/data';
import { OneRosterProviderFilter } from './OneRosterBaseType';
import { OneRosterProvider } from './OneRosterProvider';

export declare class OneRosterEnrollment extends DataObject {
    //
}

export interface OneRosterEnrollmentFilter extends OneRosterProviderFilter {
    classCode?: string;
}

export declare class OneRosterEnrollmentProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterEnrollmentFilter): Promise<any[][]>;
}

export declare class OneRosterNoEnrollmentProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterEnrollmentFilter): Promise<any[][]>;
}

export declare class OneRosterEnrollmentWithSectionsProvider extends OneRosterProvider {
    constructor(app: ApplicationBase)
    preSync(context: DataContext, filter: OneRosterEnrollmentFilter): Promise<any[][]>;
    sync(context: DataContext, filter: OneRosterEnrollmentFilter): Promise<void>;
}