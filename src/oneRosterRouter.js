import { HttpError, HttpForbiddenError } from '@themost/common';
import {Router}  from 'express';
import {OneRosterFilterParser} from './OneRosterFilterParser';
import {Passport} from 'passport';
import BearerStrategy from 'passport-http-bearer';
import { orgRouter } from './routes/orgRouter';
import { courseRouter } from './routes/courseRouter';
import { studentRouter } from './routes/studentRouter';
import { teacherRouter } from './routes/teacherRouter';
import { classRouter } from './routes/classRouter';
import { enrollmentRouter } from './routes/enrollmentRouter';
import { userRouter } from './routes/userRouter';
import { academicSessionRouter, gradingPeriodRouter, termRouter } from './routes/academicSessionRouter';
import { schoolRouter } from './routes/schoolRouter';
import { OneRosterScopeAccess } from './OneRosterScopeAccess';
import { categoryRouter, lineItemRouter } from './routes/gradebookRouter';
import { resultRouter } from './routes/resultRouter';
/**
 * @param {ExpressDataApplication} app
 */
function oneRosterRouter(app) {
    const router = Router();
    // create a new instance of Passport
    const passport = new Passport();

    const scopeAccess = new OneRosterScopeAccess(app);

    passport.use(new BearerStrategy({
        passReqToCallback: true
        }, function(req, token, done) {
            const service = req.context.getApplication().getService(function OAuth2ClientService() {});
            // if client cannot be found
            if (typeof service === 'undefined') {
                // throw configuration error
                return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
            }
            if (token == null) {
                // throw 499 Token Required error
                return done(new HttpError(499));
            }
            // get token info
            service.getTokenInfo(req.context, token).then(info => {
                if (info == null) {
                    return done(new HttpError(499));
                }
                if (!info.active) {
                    return done(new HttpError(498));
                }
                return done(null, {
                    "name": info.username,
                    "authenticationType":'Bearer',
                    "authenticationToken": token,
                    "authenticationScope": info.scope
                });
            });
      }));
    
    // use this handler to create a router context
    router.use((req, res, next) => {
        // create router context
        const newContext = app.createContext();
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    router.use((req, res, next) => {
        // set context locale from request
        req.context.locale  = req.locale;
        // set translate
        const translateService = req.context.application.getStrategy(function TranslateService() {});
        req.context.translate = function() {
            if (translateService == null) {
                return arguments[0];
            }
            return translateService.translate.apply(translateService, Array.from(arguments));
        };
        return next();
    });
    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });

    // handle rate limit (disable)
    //router.use(rateLimitHandler);

    // use bearer
    router.use(passport.authenticate('bearer', { session: false }), (req, res, next) => {
        Object.assign(req.context, {
            user: req.user
        });
        return next();
    });

    router.use((req, res, next) => {
        return scopeAccess.verify(req).then(value => {
            if (value) {
                return next();
            }
            return next(new HttpForbiddenError('Access denied due to authorization scopes.'))
        }).catch(reason => {
            return next(reason);
        });
    });

    router.get('/version', (req, res) => {
        return res.json({
            version: '1.1'
        });
    });

    router.use(function queryDecorator(req, res, next) {
        try {
            /**
         * @type {SystemQueryOptions}
         */
            const query = {
            };
            // limit
            if (Object.prototype.hasOwnProperty.call(req.query, 'limit')) {
                // set $top system query option
                Object.assign(query, {
                    $top: parseInt(req.query.limit, 10)
                });
                delete req.query.limit;
            } else {
                Object.assign(query, {
                    $top: 100
                });
            }
            // offset
            if (Object.prototype.hasOwnProperty.call(req.query, 'offset')) {
                // set $skip system query option
                Object.assign(query, {
                    $skip: parseInt(req.query.offset, 10)
                });
                delete req.query.offset;
            } else {
                Object.assign(query, {
                    $skip: 0
                });
            }
            // sort
            if (Object.prototype.hasOwnProperty.call(req.query, 'sort')) {
                let direction = 'asc';
                if (Object.prototype.hasOwnProperty.call(req.query, 'orderBy') && req.query.orderBy === 'desc') {
                    direction = 'desc';
                }
                // set $orderby system query option
                Object.assign(query, {
                    $orderby: `${req.query.sort} ${direction}`
                });
                delete req.query.sort;
            }
            // fields
            if (Object.prototype.hasOwnProperty.call(req.query, 'fields')) {
                Object.assign(query, {
                    $select: `${req.query.fields}`
                });
                delete req.query.fields;
            }
            // filter
            if (Object.prototype.hasOwnProperty.call(req.query, 'filter')) {
                const parser = new OneRosterFilterParser();
                parser.source = req.query.filter;
                parser.tokens = parser.toList();
                const filter = parser.getString();
                // set $filter system query option
                Object.assign(query, {
                    $filter: filter
                });
                delete req.query.filter;
            }
            Object.assign(req.query, {
                $top: null,
                $skip: null,
                $filter: null,
                $select: null,
                $orderby: null,
                $groupby: null
            }, query);
            return next();
        } catch (err) {
            return next(err);
        }
    });

    
    router.use('/academicSessions', academicSessionRouter(app));
    // courses
    router.use('/courses', courseRouter(app));
    // classes
    router.use('/classes', classRouter(app));
    // enrollments
    router.use('/enrollments', enrollmentRouter(app));
    // orgs
    router.use('/orgs', orgRouter(app));
    // students
    router.use('/students', studentRouter(app));
    // teachers
    router.use('/teachers', teacherRouter(app));
    // users
    router.use('/users', userRouter(app));
    // gradingPeriods
    router.use('/gradingPeriods', gradingPeriodRouter(app));
    // terms
    router.use('/terms', termRouter(app));
    // schools
    router.use('/schools', schoolRouter(app));
    // categories
    router.use('/categories', categoryRouter(app));
    // lineItems
    router.use('/lineItems', lineItemRouter(app));
    // results
    router.use('/results', resultRouter(app));
    
    return router;
}

export {
    oneRosterRouter
}