import { QueryExpression } from '@themost/query';
import { afterSave as afterSaveCourseClass } from './AfterCourseClass';

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterSave(event, callback) {
    try {
        const context = event.model.context;
        return context.model(event.model.name)
            .where('id').equal(event.target.id).select('courseClass').value().then((courseClass) => {
                if (courseClass) {
                    const { sourceAdapter: CourseClasses } = context.model('CourseClass');
                    return context.db.executeAsync(
                        new QueryExpression().update(CourseClasses).set({
                            dateModified: new Date()
                        }).where('id').equal(courseClass)
                    ).then(() => {
                        afterSaveCourseClass({
                            model: context.model('CourseClass'),
                            target: {
                                id: courseClass
                            }
                        }, (err) => {
                            if (err) {
                                return callback(err);
                            }
                            return callback();
                        });
                    });
                }
                return callback();
            }).catch((err) => {
                return callback(err);
            });
    } catch (error) {
        return callback(error);
    }
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterRemove(event, callback) {
    try {
        const context = event.model.context;
        // get previous state
        const previous = event.previous;
        // throw error if previous state is not available
        if (previous == null) {
            return callback(new Error('The previous state of the course class section cannot be determined.'));
        }
        // get courseClass
        const { courseClass } = previous;
        const { sourceAdapter: CourseClasses } = context.model('CourseClass');
        return context.db.executeAsync(
            new QueryExpression().update(CourseClasses).set({
                dateModified: new Date()
            }).where('id').equal(courseClass)
        ).then(() => {
            afterSaveCourseClass({
                model: context.model('CourseClass'),
                target: {
                    id: courseClass
                }
            }, (err) => {
                if (err) {
                    return callback(err);
                }
                return callback();
            });
        }).catch((err) => {
            return callback(err);
        });
        
    } catch (error) {
        return callback(error);
    }
}

module.exports = {
    afterSave,
    afterRemove
}