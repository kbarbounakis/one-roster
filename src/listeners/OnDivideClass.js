import { DataError } from '@themost/common';
import { QueryExpression } from '@themost/query';

 /**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    void event.model.where('id').equal(event.target.id).silent().select('courseClass', 'classIndex', 'status', 'dateModified').getItem().then((target) => {
        if (target == null) {
            return callback(new DataError('E_NOTFOUND', 'The target item cannot be found.', null, event.model.name));
        }
        let { courseClass, classIndex, status, dateModified: dateLastModified } = target;
        const context = event.model.context;
        // try to find if OneRosterClass exists with the same classCode
        const classCode = `${courseClass}-${classIndex}`;
        const { sourceAdapter: CourseClasses } = context.model('CourseClass');
        return context.db.executeAsync(
            new QueryExpression().update(CourseClasses).set({
                dateModified: new Date()
            }).where('id').equal(courseClass)
        ).then(() => {
            const OneRosterClasses = event.model.context.model('OneRosterClass').silent();
            return OneRosterClasses.where('classCode').equal(classCode).getItem().then((result) => {
                if (result == null) {
                    // OneRosterClass does not exist, so exit without doing anything
                    return callback();
                }
                // if the class has not been marked for deletion, then exit without doing anything
                status = status || 'active';
                dateLastModified = dateLastModified || new Date();
                Object.assign(result, {
                    status,
                    dateLastModified
                });
                // save changes
                return OneRosterClasses.save(result).then(() => {
                    return callback();
                });
            });
        });
    }).catch((err) => {
        return callback(err);
    });
    
 }