import { QueryExpression } from '@themost/query';
import '@themost/promise-sequence';
import { afterSave as afterSaveOneRosterClass } from './AfterOneRosterClass';
import { DataObjectState } from '@themost/data';

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterRemove(event, callback) {
    try {
        const context = event.model.context;
        // get previous state
        const previous = event.previous;
        // throw error if previous state is not available
        if (previous == null) {
            return callback(new Error('The previous state of the course class section cannot be determined.'));
        }
        // get courseClass and section
        const { id: courseClass } = previous;
        // stage #1
        // validate if OneRosterClasses are available
        const model = context.model('OneRosterClass');
        if (model == null) {
            // OneRoster classes are not available, so exit
            return callback();
        }
        // try to find OneRoster class
        return model.where('classCode').equal(courseClass).select(
            'sourcedId', 'status'
        ).silent().flatten().getItem().then((item) => {
            // if item exists
            if (item) {
                // set status to 'tobedeleted' by using an adhoc query
                const { sourceAdapter: OneRosterClasses } = model;
                return context.db.executeAsync(
                    new QueryExpression().update(OneRosterClasses).set({
                        status: 'tobedeleted',
                        dateLastModified: new Date()
                    }).where('classCode').equal(courseClass)
                ).then(() => {
                    // validate course class enrollments and delete them all
                    const model = context.model('OneRosterEnrollment');
                    if (model == null) {
                        // OneRoster enrollments are not available, so exit
                        return callback();
                    }
                    const { sourceAdapter: OneRosterEnrollments } = context.model('OneRosterEnrollment');
                    // delete class enrollments with an adhoc query
                    const { sourcedId } = item;
                    const deleteEnrollments = new QueryExpression().delete(OneRosterEnrollments).where('class').equal(sourcedId);                        
                    return context.db.executeAsync(deleteEnrollments).then(() => {
                        // call after one roster class listener
                        const target = Object.assign({}, item, {
                            status: 'tobedeleted'
                        });
                        const previous = item;
                        const state = DataObjectState.Update;
                        void afterSaveOneRosterClass({
                            previous,
                            target,
                            model,
                            state
                        }, (err) => {
                            return callback(err);
                        });
                    });
                }).catch((err) => {
                    return callback(err);
                });
            }
            // otherwise exit
            return callback();
        });
    } catch (error) {
        return callback(error);
    }
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterSave(event, callback) {
    try {
        const context = event.model.context;
        const model = context.model('OneRosterReplaceClass');
        if (model) {
            return model.where('replacedBy').equal(event.target.id).select(
                'courseClass'
            ).silent().getItems().then((items) => {
                const { sourceAdapter: CourseClasses } = context.model('CourseClass');
                return Promise.sequence(items.map(({courseClass}) => {
                    return () => {
                        return context.db.executeAsync(
                            new QueryExpression().update(CourseClasses).set({
                                dateModified: new Date()
                            }).where('id').equal(courseClass)
                        );
                    }
                })).then(() => {
                    // execute an ad-hoc query to update replacer class
                    // pseudo SQL: UPDATE CourseClasses SET dateModified = NOW() 
                    // WHERE id IN (SELECT replacedBy FROM OneRosterReplaceClasses WHERE courseClass = ?)
                    const { viewAdapter: CourseClasses } = context.model('CourseClass');
                    const { viewAdapter: OneRosterReplaceClasses } = context.model('OneRosterReplaceClass');
                    return context.db.executeAsync(
                        new QueryExpression().update(CourseClasses).set({
                            dateModified: new Date()
                        }).where('id').in(
                            new QueryExpression().select('replacedBy').from(OneRosterReplaceClasses).where('courseClass').equal(event.target.id)
                        )
                    ).then(() => {
                        return callback();
                    });
                });
            }).catch((err) => {
                return callback(err);
            });
        }
        return callback();
    } catch (error) {
        return callback(error);
    }
}

export {
    afterRemove,
    afterSave
}