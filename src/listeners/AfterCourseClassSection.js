import { QueryExpression }  from '@themost/query';
import { afterSave as afterSaveCourseClass } from './AfterCourseClass';

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterSave(event, callback) {
    try {
        const context = event.model.context;
        return context.model('CourseClassSection')
            .where('id').equal(event.target.id).select('courseClass').value().then((courseClass) => {
                if (courseClass) {
                    const { sourceAdapter: CourseClasses } = context.model('CourseClass');
                    return context.db.executeAsync(
                        new QueryExpression().update(CourseClasses).set({
                            dateModified: new Date()
                        }).where('id').equal(courseClass)
                    ).then(() => {
                        afterSaveCourseClass({
                            model: context.model('CourseClass'),
                            target: {
                                id: courseClass
                            }
                        }, (err) => {
                            if (err) {
                                return callback(err);
                            }
                            return callback();
                        });
                    });
                }
                return callback();
            }).catch((err) => {
                return callback(err);
            });
    } catch (error) {
        return callback(error);
    }
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function afterRemove(event, callback) {
    try {
        const context = event.model.context;
        // get previous state
        const previous = event.previous;
        // throw error if previous state is not available
        if (previous == null) {
            return callback(new Error('The previous state of the course class section cannot be determined.'));
        }
        // get courseClass and section
        const { courseClass, section } = previous;
        // stage #1
        // update course class dateModified
        const { sourceAdapter: CourseClasses } = context.model('CourseClass');
        return context.db.executeAsync(
            new QueryExpression().update(CourseClasses).set({
                dateModified: new Date()
            }).where('id').equal(courseClass)
        ).then(() => {
            // stage #2
            // validate if OneRosterClasses are available
            const model = context.model('OneRosterClass');
            if (model == null) {
                // OneRoster classes are not available, so exit
                return callback();
            }
            // get class code
            const classCode = `${courseClass}-${section}`;
            // try to find OneRoster class
            return model.where('classCode').equal(classCode).select(
                'sourcedId', 'status'
            ).silent().getItem().then((item) => {
                // if item exists
                if (item) {
                    // set status to 'tobedeleted' by using ab adhoc query
                    const { sourceAdapter: OneRosterClasses } = model;
                    return context.db.executeAsync(
                        new QueryExpression().update(OneRosterClasses).set({
                            status: 'tobedeleted'
                        }).where('classCode').equal(classCode)
                    ).then(() => {
                        // validate course class section enrollment and delete thema all
                        const model = context.model('OneRosterEnrollment');
                        if (model == null) {
                            // OneRoster enrollments are not available, so exit
                            return callback();
                        }
                        const { sourceAdapter: OneRosterEnrollments } = context.model('OneRosterEnrollment');
                        // delete class enrollments with an adhoc query
                        const { sourcedId } = item;
                        const deleteEnrollments = new QueryExpression().delete(OneRosterEnrollments).where('class').equal(sourcedId);                        
                        return context.db.executeAsync(deleteEnrollments).then(() => {
                            return callback();
                        });
                    });
                }
                // otherwise exit
                return callback();
            });
        }).catch((err) => {
            return callback(err);
        });
    } catch (error) {
        return callback(error);
    }
}

export {
    afterSave,
    afterRemove
};