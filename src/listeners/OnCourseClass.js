import { DataError } from '@themost/common';
import { QueryExpression } from '@themost/query';
import '@themost/promise-sequence';
/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    try {
        void event.model.where('id').equal(event.target.id).silent().select('courseClass').getItem().then((target) => {
            if (target == null) {
                return callback(new DataError('E_NOTFOUND', 'The target item cannot be found.', null, event.model.name));
            }
            const { courseClass } = target;
            const context = event.model.context;
            // update parent class (with an adhoc query)
            const { sourceAdapter: CourseClasses } = context.model('CourseClass');
            return Promise.sequence([
                () => context.db.executeAsync(
                    new QueryExpression().update(CourseClasses).set({
                        dateModified: new Date()
                    }).where('id').equal(courseClass)
                )
            ]).then(() => {
                return callback();
            });
        }).catch((err) => {
            return callback(err);
        });
    } catch (err) {
        return callback(err);
    }
 }

 /**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    try {
        if (event.previous == null) {
            return callback(new DataError('E_NOTFOUND', 'The previous state of the item cannot be found.', null, event.model.name));
        }
        const { courseClass } = event.previous;
        const context = event.model.context;
        // update parent class (with an adhoc query)
        const { sourceAdapter: CourseClasses } = context.model('CourseClass');
        return Promise.sequence([
            () => context.db.executeAsync(
                new QueryExpression().update(CourseClasses).set({
                    dateModified: new Date()
                }).where('id').equal(courseClass)
            )
        ]).then(() => {
            return callback();
        });
    } catch (error) {
        return callback(error);
    }
 }