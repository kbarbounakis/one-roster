import {OneRosterScopeAccess} from '../src/index';

describe('OneRosterScopeAccess', ()=> {
    it('should validate safe regex', async () => {
        const service = new OneRosterScopeAccess();
        const result = await service.verify({
            context: {
                user: {
                    name: 'service-account-one-roster',
                    authenticationScope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/roster-demographics.readonly'
                }
            },
            originalUrl: '/ims/oneroster/v1p1/demographics'
        });
        expect(result).toBeTruthy();
    });

    it('should validate unsafe regex', async () => {
        const service = new OneRosterScopeAccess();
        service.elements.push({
            resource: '/ims/(a|a)*$',
            scope: ['https://purl.imsglobal.org/spec/or/v1p1/scope/roster-demographics.readonly'],
            access: ['read']
        });
        await(expectAsync(service.verify({
            context: {
                user: {
                    name: 'service-account-one-roster',
                    authenticationScope: 'https://purl.imsglobal.org/spec/or/v1p1/scope/roster-demographics.readonly'
                }
            },
            originalUrl: '/ims/oneroster/v1p1/otherResources'
        }))).toBeRejectedWithError('Resource path regex is not safe. Please check your application configuration.');
    });
});